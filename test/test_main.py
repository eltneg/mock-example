import unittest
from unittest.mock import patch
from main import greet

class TestSum(unittest.TestCase):
    @patch('main.username')
    @patch('main.Classname.methodname')
    def test_sum(self, methodname, username):
        username.return_value = "World"
        methodname.return_value = "Holla "
        self.assertEqual(greet("User"), "Holla World", "Should be Holla User")
        self.assertNotEqual(greet("User"), "Hello World", "Should not be Hello User")

if __name__ == '__main__':
    unittest.main()
